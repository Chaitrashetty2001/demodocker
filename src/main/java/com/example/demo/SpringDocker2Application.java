package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDocker2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringDocker2Application.class, args);
	}

}
